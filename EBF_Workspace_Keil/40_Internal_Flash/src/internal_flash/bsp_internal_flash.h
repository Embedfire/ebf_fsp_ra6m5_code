#ifndef __BSP_INTERNAL_FLASH_H
#define	__BSP_INTERNAL_FLASH_H
#include "hal_data.h"
#include "stdio.h"


// FLASH模块测试的调试信息输出控制
#define CODE_FLASH_PRINTF_ENABLE            1
#if (1 == CODE_FLASH_PRINTF_ENABLE)
  #define CODE_FLASH_PRINTF(format, ...)    printf("[Code FLASH Operation] "format"\r\n", ##__VA_ARGS__)
#else
  #define CODE_FLASH_PRINTF(format, ...)
#endif

#define DATA_FLASH_PRINTF_ENABLE            1
#if (1 == DATA_FLASH_PRINTF_ENABLE)
  #define DATA_FLASH_PRINTF(format, ...)    printf("[Data FLASH Operation] "format"\r\n", ##__VA_ARGS__)
#else
  #define DATA_FLASH_PRINTF(format, ...)
#endif


/* Code Flash */
#define FLASH_HP_CF_BLOCK_SIZE_32KB (32*1024)   /* Block Size 32 KB */
#define FLASH_HP_CF_BLOCK_SIZE_8KB  (8*1024)    /* Block Size 8KB */

#define FLASH_HP_CF_BLOCK_0         0x00000000U /*   8 KB: 0x00000000 - 0x00001FFF */
#define FLASH_HP_CF_BLOCK_1         0x00002000U /*   8 KB: 0x00002000 - 0x00003FFF */
#define FLASH_HP_CF_BLOCK_2         0x00004000U /*   8 KB: 0x00004000 - 0x00005FFF */
#define FLASH_HP_CF_BLOCK_3         0x00006000U /*   8 KB: 0x00006000 - 0x00007FFF */
#define FLASH_HP_CF_BLOCK_4	        0x00008000U /*   8 KB: 0x00008000 - 0x00009FFF */
#define FLASH_HP_CF_BLOCK_5	        0x0000A000U /*   8 KB: 0x0000A000 - 0x0000BFFF */
#define FLASH_HP_CF_BLOCK_6	        0x0000C000U /*   8 KB: 0x0000C000 - 0x0000DFFF */
#define FLASH_HP_CF_BLOCK_7	        0x0000E000U /*   8 KB: 0x0000E000 - 0x0000FFFF */
#define FLASH_HP_CF_BLOCK_8         0x00010000U /*  32 KB: 0x00010000 - 0x00017FFF */
#define FLASH_HP_CF_BLOCK_9         0x00018000U /*  32 KB: 0x00018000 - 0x0001FFFF */
#define FLASH_HP_CF_BLOCK_10        0x00020000U /*  32 KB: 0x00020000 - 0x00027FFF */
#define FLASH_HP_CF_BLOCK_11        0x00028000U /*  32 KB: 0x00020000 - 0x00027FFF */
#define FLASH_HP_CF_BLOCK_12        0x00030000U /*  32 KB: 0x00020000 - 0x00027FFF */
#define FLASH_HP_CF_BLOCK_13        0x00038000U /*  32 KB: 0x00020000 - 0x00027FFF */
#define FLASH_HP_CF_BLOCK_14        0x00040000U /*  32 KB: 0x00020000 - 0x00027FFF */
//......


/* Data Flash */
#define FLASH_HP_DF_BLOCK_SIZE      (64)        /* Block Size 64B */

#define FLASH_HP_DF_BLOCK_0         0x08000000U /* 大小为 64 B:  0x08000000U - 0x0800003F */
#define FLASH_HP_DF_BLOCK_1         0x08000040U /* 大小为 64 B:  0x08000040U - 0x0800007F */
#define FLASH_HP_DF_BLOCK_2         0x08000080U /* 大小为 64 B:  0x08000080U - 0x080000BF */
#define FLASH_HP_DF_BLOCK_3         0x080000C0U /* 大小为 64 B:  0x080000C0U - 0x080000FF */


/* CODE FLASH 测试 */
#define CODE_FLASH_TEST_BLOCK       0x00100000U /* 避免覆盖程序代码数据 */
#define CODE_FLASH_TEST_DATA_SIZE   128         /* 只测试128个字节数据 */
/* DATA FLASH 测试 */
#define DATA_FLASH_TEST_BLOCK       FLASH_HP_DF_BLOCK_0
#define DATA_FLASH_TEST_DATA_SIZE   FLASH_HP_DF_BLOCK_SIZE  //测试64B

/* 擦除的 CODE/DATA FLASH 的块数 */
#define BLOCK_NUM   1   //2


void FLASH_HP_Init(void);
void FLASH_HP_CodeFlash_Operation(void);
void FLASH_HP_DataFlash_Operation(void);

#endif

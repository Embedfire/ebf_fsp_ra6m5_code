#ifndef MY_NSC_API_MY_NSC_API_H_
#define MY_NSC_API_MY_NSC_API_H_
#include "hal_data.h"

/*自定义的非安全可调用函数*/
BSP_CMSE_NONSECURE_ENTRY void My_nsc_api_Init(void);
BSP_CMSE_NONSECURE_ENTRY uint8_t My_nsc_api_apply(void);


#endif /* MY_NSC_API_MY_NSC_API_H_ */

#ifndef __NSC_BSP_UART_H__
#define __NSC_BSP_UART_H__

#include "hal_data.h"
#include "stdio.h"

void debug_uart4_callback (uart_callback_args_t * p_args);
void Debug_UART4_Init(void);

#endif /* UART_BSP_UART_H_ */

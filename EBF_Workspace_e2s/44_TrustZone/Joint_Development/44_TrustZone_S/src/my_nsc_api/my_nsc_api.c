#include "my_nsc_api.h"
#include "gpt/bsp_gpt_pwm_output.h"
#include "adc/bsp_adc.h"
/*初始化 GPT,ADC模块*/
BSP_CMSE_NONSECURE_ENTRY void My_nsc_api_Init(void)
{
  GPT_PWM_Init();
  ADC_Init();
}

BSP_CMSE_NONSECURE_ENTRY uint8_t My_nsc_api_apply(void)
{
  uint8_t temp;
  /*将读到的电压值进行处理，使其不能超过GPT_PWM_SetDuty()输入PWM占空比范围*/
  temp = (uint8_t)(Read_ADC_Voltage_Value()*100/3.3);
  GPT_PWM_SetDuty(temp);
  /*返回输出PWM占空比*/
  return temp;
}

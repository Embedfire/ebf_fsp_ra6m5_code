#ifndef BSP_SPI_OLED_H_
#define BSP_SPI_OLED_H_

#include "hal_data.h"

#define XLevelL			0x00
#define XLevelH			0x10
#define XLevel	    ((XLevelH&0x0F)*16+XLevelL)
#define Max_Column	128
#define Max_Row			64
#define	Brightness	0xCF 
#define X_WIDTH 		128
#define Y_WIDTH 		64

#define OLED_CS_Pin       BSP_IO_PORT_03_PIN_01
#define OLED_CS_Clr()     R_IOPORT_PinWrite(&g_ioport_ctrl, OLED_CS_Pin, BSP_IO_LEVEL_LOW)
#define OLED_CS_Set()     R_IOPORT_PinWrite(&g_ioport_ctrl, OLED_CS_Pin, BSP_IO_LEVEL_HIGH)

#define OLED_DC_Pin       BSP_IO_PORT_03_PIN_02
#define OLED_DC_Clr()     R_IOPORT_PinWrite(&g_ioport_ctrl, OLED_DC_Pin, BSP_IO_LEVEL_LOW)
#define OLED_DC_Set()     R_IOPORT_PinWrite(&g_ioport_ctrl, OLED_DC_Pin, BSP_IO_LEVEL_HIGH)

//#define OLED_CMD  0	//写命令
//#define OLED_DATA 1	//写数据
//OLED控制用函数
void OLED_WrDat(unsigned char dat);//写数据
void OLED_WrCmd(unsigned char cmd);//写命令
void OLED_SetPos(unsigned char x, unsigned char y);//设置起始点坐标
void OLED_Fill(unsigned char bmp_dat);//全屏填充
void OLED_CLS(void);//清屏
void OLED_Init(void);//初始化
void OLED_6x8Str(unsigned char x, unsigned char y, unsigned char ch[]);
void OLED_8x16Str(unsigned char x, unsigned char y, unsigned char ch[]);
void OLED_16x16CN(unsigned char x, unsigned char y, unsigned char N);
void OLED_BMP(unsigned char x0, unsigned char y0, unsigned char x1, unsigned char y1, unsigned char BMP[]);

#endif 